package com.appsoluut.example.android;

import android.util.Log;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

import java.util.ArrayList;
import java.util.List;

public class PhysicsWorld {
    public static final int TARGET_FPS = 30;
    public static final int TIME_STEP = (1000 / TARGET_FPS);
    public static final int VEL_ITER = 2;
    public static final int POS_ITER = 5;

    private List<Body> bodies = new ArrayList<Body>();

    private World world;

    public PhysicsWorld() {
        recreateWorld(16.0f, 12.0f);
    }

    public void recreateWorld(float xmax, float ymax) {
        Log.i("World", "RecreateWorld " + xmax + " / " + ymax);
        bodies.clear();

        Vec2 gravity = new Vec2(0.0f, 10.0f);
        world = new World(gravity, true);

        BodyDef def = new BodyDef();
        def.type = BodyType.STATIC;
        Body groundBody = world.createBody(def);
        createEdge(0.0f, 0.0f, xmax, 0.0f, groundBody);
        createEdge(xmax, 0.0f, xmax, ymax, groundBody);
        createEdge(xmax, ymax, 0.0f, ymax, groundBody);
        createEdge(0.0f, ymax, 0.0f, 0.0f, groundBody);
    }

    private void createEdge(float x1, float y1, float x2, float y2,
                            Body groundBody) {
        Vec2 v1 = new Vec2(x1, y1);
        Vec2 v2 = new Vec2(x2, y2);
        Log.i("World", "Add edge " + v1 + " / " + v2);

        PolygonShape groundShapeDef = new PolygonShape();
        groundShapeDef.setAsEdge(v1, v2);

        groundBody.createFixture(groundShapeDef, 0.0f);
    }

    public void addBall(Vec2 position, float radius) {
        Log.i("World", "Add ball " + position + " / " + radius);

        CircleShape shape   = new CircleShape();
        shape.m_radius      = radius;
        
        BodyDef bodyDef     = new BodyDef();
        bodyDef.type        = BodyType.DYNAMIC;
        bodyDef.position.set(position);
        bodyDef.allowSleep  = false;

        Body body           = world.createBody(bodyDef);
        body.createFixture(shape, 1.0f);

        bodies.add(body);
    }

    public List<Body> getBalls() {
        return bodies;
    }

    public void update() {
        // Log.i("World", "World update");
        world.step(TIME_STEP, VEL_ITER, POS_ITER);
    }
}