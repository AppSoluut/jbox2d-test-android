package com.appsoluut.example.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class JBox2DTest extends Activity
{
    PhysicsWorld    mWorld;
    BallView        mView;
    private Handler mHandler;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        mView = new BallView(this);
        setContentView(mView);
        
        //setContentView(this);
        //setContentView(R.layout.main);

        mWorld = new PhysicsWorld();
        mWorld.recreateWorld(dm.widthPixels, dm.heightPixels);
        mWorld.addBall(new Vec2(20f, 100f), 20);
        mWorld.addBall(new Vec2(100f, 100f), 40);
        mWorld.addBall(new Vec2(60f, 30f), 30);
        //mWorld.addBall();

        mHandler = new Handler();
        mHandler.post(mDrawCube);
    }

    private final Runnable mDrawCube = new Runnable() {
        public void run() {
            // Log.i("Wallpaper", "mDrawCube run");
            //mHandler.removeCallbacks(mDrawCube);

            mWorld.update();
            mHandler.postDelayed(mDrawCube, PhysicsWorld.TIME_STEP);
            mView.invalidate();
        }
    };

    private class BallView extends View
    {
        private Paint mBallPaintActive;
        private Paint mBallPaintInactive;
        
        public BallView(Context context) {
            super(context);

            mBallPaintActive = new Paint(Paint.ANTI_ALIAS_FLAG);
            mBallPaintActive.setColor(Color.GREEN);

            mBallPaintInactive = new Paint(Paint.ANTI_ALIAS_FLAG);
            mBallPaintInactive.setColor(Color.RED);
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);
            
            canvas.drawColor(Color.WHITE);
            
            for(Body ball : mWorld.getBalls())
            {
                Paint ballPaint = mBallPaintInactive;
                if(ball.isAwake())
                {
                    ballPaint = mBallPaintActive;
                }

                Vec2 pos = ball.getPosition();
                canvas.drawCircle(pos.x, pos.y, ball.getFixtureList().getShape().m_radius, ballPaint);
            }
        }
    }
}
